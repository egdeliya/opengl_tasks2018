#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>

class SampleApplication : public Application
{
public:
	MeshPtr _ground;

	ShaderProgramPtr _shader;


	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();

		_ground = makeGrid(50, 50 * 4);

		_shader = std::make_shared<ShaderProgram>("496NurgalievaData/shader.vert", "496NurgalievaData/shader.frag");
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix",
			glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
		_ground->draw();
		//_ground->drawWithGrid();
	}
};

int main()
{
	SampleApplication app;
	app.start();
	system("pause");
	return 0;
}