#pragma once

#include <vector>
#include <PerlinNoise.h>

class HeightGridGenerator {
public:

	HeightGridGenerator(int rows, int cols, double persistence, double frequency, double amplitude, int octaves, int randomseed) :
		_heightGrid(rows, std::vector<float>(cols)),
		_perlinNoiseGenerator(persistence, frequency, amplitude, octaves, randomseed),
		_rows(rows),
		_cols(cols)
	{
		initHeightGrid(_cols, _rows);
	}

	HeightGridGenerator(double persistence, double frequency, double amplitude, int octaves, int randomseed) :
		_perlinNoiseGenerator(persistence, frequency, amplitude, octaves, randomseed) {}


	float getHeight(int col, int row);
	void initHeightGrid(int col, int row);

private:


	PerlinNoise _perlinNoiseGenerator;
	std::vector<std::vector<float>> _heightGrid;
	int _cols, _rows;
};