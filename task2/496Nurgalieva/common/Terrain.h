#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include <glm/glm.hpp>

#include <ctime>
#include <random>
#include <PerlinNoise.h>
#include <memory>
#include <math.h>

class Terrain
{
public:
	Terrain();

	Terrain(int size, int squaresNumber, int tilesNumber);

	float getTerrainHeight(float x, float y) const;

	float getHeight(int x_ind, int y_ind) const;

	int getSize() const;

	int getSquaresNum() const;
	
	int getTilesNum() const;

private:

	float barrycentricCoord(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) const;

	std::mt19937 _gen;
	int _size;
	int _squaresNum;

	PerlinNoise _perlinNoiseGenerator;
	std::vector<std::vector<float>> _heightGrid;
	int _colsNum, _rowsNum, _tilesNumber;

};

using TerrainPtr = std::shared_ptr<Terrain>;