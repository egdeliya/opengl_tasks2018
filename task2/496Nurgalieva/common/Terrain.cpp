#include <Terrain.h>

Terrain::Terrain() :
	Terrain(64, 64, 512)
{}

Terrain::Terrain(int size, int squaresNumber, int tilesNumber) :
	_size(size),
	_squaresNum(squaresNumber),
	_heightGrid(squaresNumber + 1, std::vector<float>(squaresNumber + 1)),
	_tilesNumber(tilesNumber)
{
	_gen.seed(time(0));
	_perlinNoiseGenerator.setPersistence(0.3);
	_perlinNoiseGenerator.setFrequency(0.03);
	_perlinNoiseGenerator.setAmplitude(2);
	_perlinNoiseGenerator.setOctaves(20);
	_perlinNoiseGenerator.setRandomSeed(_gen() / _gen.max());
	
	// heigh grid init
	for (int row = 0; row < squaresNumber + 1; row++) {
		for (int col = 0; col < squaresNumber + 1; col++) {
			_heightGrid[row][col] = _perlinNoiseGenerator.getHeight(row, col);
		}
	}
}

float Terrain::getTerrainHeight(float x, float y) const {
	// ������ ������ ����������
	float squareSize = _size * 1. / _squaresNum;
		
	// ���������, � ����� ���������� ��������� ������
	int gridXind = floor(x / squareSize);
	int gridYind = floor(y / squareSize);

	// �������� �� ����� �� ������� �������
	if (gridYind < 0 || gridYind > _squaresNum || gridXind < 0 || gridXind > _squaresNum) {
		return 0;
	}

	// ��������� ���������� ������ � ����������
	float xGridCoord = glm::mod(x, squareSize) / squareSize;
	float yGridCoord = glm::mod(y, squareSize) / squareSize;

	float height;
	// � ����������� �� ����, � ����� ������������ ��������� ������, ����� ������
	if (xGridCoord < 1 - yGridCoord) {
		return barrycentricCoord(glm::vec3(0, 0, _heightGrid[gridXind][gridYind]),
			glm::vec3(1, 0, _heightGrid[gridXind + 1][gridYind]),
			glm::vec3(0, 1, _heightGrid[gridXind][gridYind + 1]),
			glm::vec2(yGridCoord, xGridCoord));
	}
	else {
		return barrycentricCoord(glm::vec3(1, 0, _heightGrid[gridXind + 1][gridYind]),
			glm::vec3(1, 1, _heightGrid[gridXind + 1][gridYind + 1]),
			glm::vec3(0, 1, _heightGrid[gridXind][gridYind + 1]),
			glm::vec2(yGridCoord, xGridCoord));
	}
}

// TODO ������� ��������� ������ �� �������
float Terrain::getHeight(int x_ind, int y_ind) const {
	
	//if (x_ind < 0 || x_ind > _size || y_ind < 0 || y_ind > _size) return -1;
	return _heightGrid[x_ind][y_ind];
}


int Terrain::getSize() const {
	return _size;
}

int Terrain::getTilesNum() const {
	return _tilesNumber;
}


int Terrain::getSquaresNum() const {
	return _squaresNum;
}

float Terrain::barrycentricCoord(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) const {
	float det = (p2.x - p3.x) * (p1.y - p3.y) + (p3.y - p2.y) * (p1.x - p3.x);
	float l1 = ((p2.x - p3.x) * (pos.x - p3.y) + (p3.y - p2.y) * (pos.y - p3.x)) / det;
	float l2 = ((p3.x - p1.x) * (pos.x - p3.y) + (p1.y - p3.y) * (pos.y - p3.x)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * p1.z + l2 * p2.z + l3 * p3.z;
}