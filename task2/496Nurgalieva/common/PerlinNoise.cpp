#include "PerlinNoise.h"

PerlinNoise::PerlinNoise() :
	_persistence(0),
	_frequency(0),
	_amplitude(0),
	_octaves(0),
	_randomseed(0) 
	{}

PerlinNoise::PerlinNoise(double persistence, double frequency, double amplitude, int octaves, int randomseed):
	_persistence(persistence),
	_frequency(frequency),
	_amplitude(amplitude),
	_octaves(octaves),
	_randomseed(2 + randomseed * randomseed)
{}

void PerlinNoise::set(double persistence, double frequency, double amplitude, int octaves, int randomseed)
{
	_persistence = persistence;
	_frequency = frequency;
	_amplitude = amplitude;
	_octaves = octaves;
	_randomseed = 2 + randomseed * randomseed;
}

double PerlinNoise::getHeight(double x, double y) const
{
	return _amplitude * total(x, y);
}

double PerlinNoise::total(double i, double j) const
{
	//properties of one octave (changing each loop)
	double t = 0.0f;
	double amplitude = 1;
	double freq = _frequency;

	for (int k = 0; k < _octaves; k++)
	{
		t += getValue(j * freq + _randomseed, i * freq + _randomseed) * amplitude;
		amplitude *= _persistence;
		freq *= 2;
	}

	return t;
}

double PerlinNoise::getValue(double x, double y) const
{
	int xInt = (int)x;
	int yInt = (int)y;
	double xFrac = x - xInt;
	double yFrac = y - yInt;

	//noise values
	double n01 = noise(xInt - 1, yInt - 1);
	double n02 = noise(xInt + 1, yInt - 1);
	double n03 = noise(xInt - 1, yInt + 1);
	double n04 = noise(xInt + 1, yInt + 1);
	double n05 = noise(xInt - 1, yInt);
	double n06 = noise(xInt + 1, yInt);
	double n07 = noise(xInt, yInt - 1);
	double n08 = noise(xInt, yInt + 1);
	double n09 = noise(xInt, yInt);

	double n12 = noise(xInt + 2, yInt - 1);
	double n14 = noise(xInt + 2, yInt + 1);
	double n16 = noise(xInt + 2, yInt);

	double n23 = noise(xInt - 1, yInt + 2);
	double n24 = noise(xInt + 1, yInt + 2);
	double n28 = noise(xInt, yInt + 2);

	double n34 = noise(xInt + 2, yInt + 2);

	//find the noise values of the four corners
	double x0y0 = 0.0625*(n01 + n02 + n03 + n04) + 0.125*(n05 + n06 + n07 + n08) + 0.25*(n09);
	double x1y0 = 0.0625*(n07 + n12 + n08 + n14) + 0.125*(n09 + n16 + n02 + n04) + 0.25*(n06);
	double x0y1 = 0.0625*(n05 + n06 + n23 + n24) + 0.125*(n03 + n04 + n09 + n28) + 0.25*(n08);
	double x1y1 = 0.0625*(n09 + n16 + n28 + n34) + 0.125*(n08 + n14 + n06 + n24) + 0.25*(n04);

	//interpolate between those values according to the x and y fractions
	double v1 = interpolate(x0y0, x1y0, xFrac); //interpolate in x direction (y)
	double v2 = interpolate(x0y1, x1y1, xFrac); //interpolate in x direction (y+1)
	double fin = interpolate(v1, v2, yFrac);  //interpolate in y direction

	return fin;
}

double PerlinNoise::interpolate(double x, double y, double a) const
{
	double negA = 1.0 - a;
	double negASqr = negA * negA;
	double fac1 = 3.0 * (negASqr)-2.0 * (negASqr * negA);
	double aSqr = a * a;
	double fac2 = 3.0 * aSqr - 2.0 * (aSqr * a);

	return x * fac1 + y * fac2; //add the weighted factors
}

double PerlinNoise::noise(int x, int y) const
{
	int n = x + y * 57;
	n = (n << 13) ^ n;
	int t = (n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff;
	return 1.0 - double(t) * 0.931322574615478515625e-9;/// 1073741824.0);
}
