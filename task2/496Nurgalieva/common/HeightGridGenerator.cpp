#include <HeightGridGenerator.h>

void HeightGridGenerator::initHeightGrid(int cols, int rows) {
    _rows = rows, _cols = cols;
	for (int row = 0; row < rows; row++) {
		for (int col = 0; col < cols; col++) {
			_heightGrid[row][col] = _perlinNoiseGenerator.getHeight(row, col);
		}
	}
}

float HeightGridGenerator::getHeight(int col, int row) const {
	return _heightGrid[col][row];
}
