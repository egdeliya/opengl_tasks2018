#pragma once

class PerlinNoise
{
public:

	PerlinNoise();
	PerlinNoise(double persistence, double frequency, double amplitude, int octaves, int randomseed);

	double getHeight(double x, double y) const;

	double getPersistence() const { 
		return _persistence; 
	}

	double getFrequency() const {
		return _frequency; 
	}

	double getAmplitude() const { 
		return _amplitude; 
	}

	int getOctaves() const { 
		return _octaves; 
	}

	int getRandomSeed() const { 
		return _randomseed; 
	}

	void set(double persistence, double frequency, double amplitude, int octaves, int randomseed);

	void setPersistence(double persistence) { 
		_persistence = persistence; 
	}
	void setFrequency(double frequency) { 
		_frequency = frequency;
	}
	void setAmplitude(double amplitude) { 
		_amplitude = amplitude;
	}
	void setOctaves(int octaves) { 
		_octaves = octaves;
	}
	void setRandomSeed(int randomseed) { 
		_randomseed = randomseed;
	}

private:

	double total(double i, double j) const;
	double getValue(double x, double y) const;
	double interpolate(double x, double y, double a) const;
	double noise(int x, int y) const;

	double _persistence, _frequency, _amplitude;
	int _octaves, _randomseed;
};
