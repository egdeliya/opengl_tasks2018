#version 330

uniform sampler2D grassTex;
uniform sampler2D mudTex;
uniform sampler2D pathTex;
//uniform sampler2D snowTex;
uniform sampler2D rockTex;
uniform sampler2D maskTex;

struct LightInfo
{
    vec3 dir; 
    vec3 La; //���� � ������������� ����������� �����
    vec3 Ld; //���� � ������������� ���������� �����
	vec3 Ls; 
};
uniform LightInfo light;

in vec3 normalCamSpace; //������� � ������� ��������� ������ (��������������� ����� ��������� ������������)
in vec4 posCamSpace; //���������� ������� � ������� ��������� ������ (��������������� ����� ��������� ������������)
in vec3 vertNormal; // ������� � ������ �������
in vec2 texCoord; //���������� ���������� (��������������� ����� ��������� ������������)
out vec4 fragColor;

uniform int tilesNumber;
//uniform float squaresNumber;

const vec3 grassKs = vec3(0.3, 0.3, 0.3); //����������� ��������� ���������
const vec3 groundKs = vec3(0.3, 0.3, 0.3); //����������� ��������� ���������
const vec3 sendKs = vec3(0.5, 0.5, 0.5); //����������� ��������� ���������
const vec3 snowKs = vec3(0.4, 0.4, 0.4); //����������� ��������� ���������
const float shininess = 300.0;

void main()
{
	
	vec3 normal = normalize(vertNormal);
	vec3 viewDirection = normalize(-posCamSpace.xyz);

	// --------------- �������� ����� �� ������� -------------------
	vec4 blendMapColor = texture(maskTex, texCoord);

	float backgroungAmount = 1.0 - (blendMapColor.r + blendMapColor.g + blendMapColor.b);
	vec4 grassTextColor = texture(grassTex, texCoord * tilesNumber) * backgroungAmount;
	vec4 mudTextColor = texture(mudTex, texCoord * tilesNumber) * blendMapColor.r;
	vec4 pathTextColor = texture(pathTex, texCoord * tilesNumber) * blendMapColor.b;
	vec4 rockTextColor = texture(rockTex, texCoord * tilesNumber) * blendMapColor.g;
	vec3 finalColor = (grassTextColor + mudTextColor + pathTextColor + rockTextColor).rgb;

	float NdotL = max(dot(vertNormal, light.dir), 0.0);
	fragColor.rgb = finalColor.rgb * (light.La + light.Ld * NdotL); 

	// -------------- ������� ����� -----------------
	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(light.dir + viewDirection);
		float blinnTerm = max(dot(normal, halfVector), 0.0);			
		blinnTerm = pow(blinnTerm, shininess);
		fragColor.rgb += light.Ls * (grassKs * backgroungAmount + groundKs * blendMapColor.r + sendKs * blendMapColor.b  + snowKs * blendMapColor.g) * blinnTerm;
	}

	fragColor.a = 1.0;
}
