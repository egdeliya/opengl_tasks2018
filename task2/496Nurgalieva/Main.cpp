#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <Terrain.h>
#include <Texture.hpp>

class TerrainApplication : public Application
{
public:
	MeshPtr _ground;
	ShaderProgramPtr _shader;

	// ���������� ���������
	TexturePtr _grassTex;
	TexturePtr _mudTex;
	TexturePtr _pathTex;
	TexturePtr _snowTex;
	TexturePtr _rockTex;
	TexturePtr _maskTex;

	GLuint _samplerGrass;
	GLuint _samplerMud;
	GLuint _samplerPath;
	GLuint _samplerSnow;
	GLuint _samplerRock;
	GLuint _samplerMask;

	//���������� ��������� �����
	float _phi = glm::pi<float>() * 0.75f;
	float _theta = glm::pi<float>() * 0.75f;

	//��������� ��������� �����
	glm::vec3 _lightAmbientColor;
	glm::vec3 _lightDiffuseColor;
	glm::vec3 _lightSpecularColor;

	void makeScene() override
	{
		Application::makeScene();

		// size, squaresNumber, tilesNumber
		_terrain = std::make_shared<Terrain>(32, 512, 64);
		_cameraMover = std::make_shared<FreeCameraTerrainMover>(_terrain);

		_ground = makeGridCopy(_terrain);

		_shader = std::make_shared<ShaderProgram>("496NurgalievaData/shader.vert", "496NurgalievaData/shader.frag");

		//=========================================================
		//������������� �������� ���������� ��������
		_lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
		_lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
		_lightSpecularColor = glm::vec3(0.5, 0.5, 0.5);

		//=========================================================
		//�������� � �������� �������
		_grassTex = loadTexture("496NurgalievaData/grassy.jpg");
		_mudTex = loadTexture("496NurgalievaData/lava.jpg");
		_pathTex = loadTexture("496NurgalievaData/ground.jpg");
		_snowTex = loadTexture("496NurgalievaData/snow.jpg");
		_rockTex = loadTexture("496NurgalievaData/rock2.jpg");
		_maskTex = loadTexture("496NurgalievaData/mask6.png");

		//=========================================================
		//������������� ��������, �������, ������� ������ ��������� ������ �� ��������
		glGenSamplers(1, &_samplerGrass); // grass
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerMud); // mud
		glSamplerParameteri(_samplerMud, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerMud, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerMud, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerMud, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerPath); //path
		glSamplerParameteri(_samplerPath, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerPath, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerPath, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerPath, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerRock); //snow
		glSamplerParameteri(_samplerRock, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerRock, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerRock, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerRock, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerMask); //mask
		glSamplerParameteri(_samplerMask, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerMask, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_T, GL_REPEAT);
		//glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// new new new new
		glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));

		_shader->use();

		_shader->setVec3Uniform("light.dir", lightDir);
		_shader->setVec3Uniform("light.La", _lightAmbientColor);
		_shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
		_shader->setVec3Uniform("light.Ls", _lightSpecularColor);

		//================ Texturing ================
		GLuint textureUnitForGrassTex = 0;
		glBindSampler(textureUnitForGrassTex, _samplerGrass);
		glActiveTexture(GL_TEXTURE0);  //���������� ���� 0 grass
		_grassTex->bind();
		_shader->setIntUniform("grassTex", textureUnitForGrassTex);

		GLuint textureUnitForPathTex = 1;
		glBindSampler(textureUnitForPathTex, _samplerPath);
		glActiveTexture(GL_TEXTURE1);  //���������� ���� 1 path
		_pathTex->bind();
		_shader->setIntUniform("pathTex", textureUnitForPathTex);

		GLuint textureUnitForRockTex = 2;
		glBindSampler(textureUnitForRockTex, _samplerRock);
		glActiveTexture(GL_TEXTURE2);  //���������� ���� 2 rock
		_rockTex->bind();
		_shader->setIntUniform("rockTex", textureUnitForRockTex);

		GLuint textureUnitForMaskTex = 3;
		glBindSampler(textureUnitForMaskTex, _samplerMask);
		glActiveTexture(GL_TEXTURE3);  //���������� ���� 3 mask
		_maskTex->bind();
		_shader->setIntUniform("maskTex", textureUnitForMaskTex);

		GLuint textureUnitForMudTex = 4;
		glBindSampler(textureUnitForMudTex, _samplerMud);
		glActiveTexture(GL_TEXTURE4);  //���������� ���� 4 mud
		_mudTex->bind();
		_shader->setIntUniform("mudTex", textureUnitForMudTex);

		_shader->setIntUniform("tilesNumber", _terrain->getTilesNum());
		//_shader->setFloatUniform("squaresNumber", _terrain->getSquaresNum());

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix",
			glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
		_ground->draw();

		//����������� ������� � ��������� ���������
		glBindSampler(0, 0);
		glBindSampler(4, 0);
		glBindSampler(3, 0);
		glBindSampler(1, 0);
		glBindSampler(2, 0);

		glUseProgram(0);
	}

private:

	TerrainPtr _terrain;

};

int main()
{
	TerrainApplication app;
	app.start();
	//system("pause");
	return 0;
}