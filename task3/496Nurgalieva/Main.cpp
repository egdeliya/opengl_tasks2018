#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <Terrain.h>
#include <Texture.hpp>
#include <chrono>

class TerrainApplication : public Application
{
public:
	MeshPtr _ground;
	ShaderProgramPtr _shader;

	// ���������� ���������
	TexturePtr _grassTex;
	TexturePtr _mudTex;
	TexturePtr _pathTex;
	TexturePtr _snowTex;
	TexturePtr _rockTex;
	TexturePtr _maskTex;

	// --------- ��� ���� ----------
	MeshPtr _backgroundCube;
	ShaderProgramPtr _skyboxShader;
	TexturePtr _cubeTex;
	GLuint _cubeTexSampler;

	float skyRotationSpeed = 0.003;
	float skyRotation = 0.0;
	// ----------------------------

	// -------- ��� ���� ------
	MeshPtr _quad;
	ShaderProgramPtr _quadShader;
	bool _showDebugQuadReflection = false;
	bool _showDebugQuadReflectionNorm = false;
	bool _showDebugQuadRefraction = false;

	MeshPtr _water;
	ShaderProgramPtr _waterShader;

	TexturePtr _refractionTex;
	TexturePtr _reflectionTex;
	TexturePtr _reflectionNormTex;
	TexturePtr _dudvMapTex;

	GLuint _samplerWater;

	float _move = 0.0;
	float _moveSpeed = 0.0009;
	//-------------------------

	//------- Frame buffers -------------
	GLuint _renderRefractionTexId;
	GLuint _renderReflectionTexId;
	GLuint _renderReflectionNormTexId;

	GLuint _framebufferRefractionId;
	GLuint _framebufferReflectionId;
	GLuint _framebufferReflectionNormId;

	unsigned int _fbWidth = 1700;
	unsigned int _fbHeight = 1700;

	// ����������� ������ ��� ���������
	CameraMoverPtr _fbReflectionCamera;
	//---------------------------------

	GLuint _samplerGrass;
	GLuint _samplerMask;

	//���������� ��������� �����
	float _phi = glm::pi<float>() * 0.75f;
	float _theta = glm::pi<float>() * 0.75f;

	//��������� ��������� �����
	glm::vec3 _lightAmbientColor;
	glm::vec3 _lightDiffuseColor;
	glm::vec3 _lightSpecularColor;

	void initRefractionFramebuffer_noDSA() {
		//������� ����������
		glGenFramebuffers(1, &_framebufferRefractionId);
		glBindFramebuffer(GL_FRAMEBUFFER, _framebufferRefractionId);
		//----------------------------

		//������� ��������, ���� ����� �������������� ���������
		glGenTextures(1, &_renderRefractionTexId);
		glBindTexture(GL_TEXTURE_2D, _renderRefractionTexId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _renderRefractionTexId, 0);

		//----------------------------

		//������� ����� ������� ��� �����������
		GLuint depthTex;
		//������� ��������, ���� ����� �������������� ���������
		glGenTextures(1, &depthTex);
		glBindTexture(GL_TEXTURE_2D, depthTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, _fbWidth, _fbHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

		//----------------------------

		//��������� ���� ������ �� ����� ���������
		GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, buffers);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			std::cerr << "Failed to setup framebuffer\n";
			exit(1);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void initReflectionFramebuffer_noDSA(GLuint& _renderTexId, GLuint& _framebufferId) {

		//������� ����������
		glGenFramebuffers(1, &_framebufferId);
		glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);
		//----------------------------

		//������� ��������, ���� ����� �������������� ���������
		glGenTextures(1, &_renderTexId);
		glBindTexture(GL_TEXTURE_2D, _renderTexId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _renderTexId, 0);

		//----------------------------

		//������� ����� ������� ��� �����������
		GLuint depthRenderBuffer;
		glGenRenderbuffers(1, &depthRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _fbWidth, _fbHeight);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

		//----------------------------

		//��������� ���� ������ �� ����� ���������
		GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, buffers);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			std::cerr << "Failed to setup framebuffer\n";
			exit(1);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void makeScene() override
	{
		Application::makeScene();

		// ----------------- ���� ---------------------
		_backgroundCube = makeCube(128.0f);
		_skyboxShader = std::make_shared<ShaderProgram>("496NurgalievaData/skybox.vert", "496NurgalievaData/skybox.frag");
		_cubeTex = loadCubeTexture("496NurgalievaData/skybox");

		// ------------------- ���� ---------------------
		_water = makeGroundPlane(32);
		_waterShader = std::make_shared<ShaderProgram>("496NurgalievaData/water.vert", "496NurgalievaData/water.frag");

		//������������� �����������
		initRefractionFramebuffer_noDSA();
		initReflectionFramebuffer_noDSA(_renderReflectionTexId, _framebufferReflectionId);
		initReflectionFramebuffer_noDSA(_renderReflectionNormTexId, _framebufferReflectionNormId);

		_reflectionTex = std::make_shared<Texture>(_renderReflectionTexId, GL_TEXTURE_2D);
		_refractionTex = std::make_shared<Texture>(_renderRefractionTexId, GL_TEXTURE_2D);
		_reflectionNormTex = std::make_shared<Texture>(_renderReflectionNormTexId, GL_TEXTURE_2D);

		_dudvMapTex = loadTexture("496NurgalievaData/waterDUDV.png");
		//--------------------------------

		glGenSamplers(1, &_cubeTexSampler);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		_quad = makeScreenAlignedQuad();
		_quadShader = std::make_shared<ShaderProgram>("496NurgalievaData/quadColor.vert", "496NurgalievaData/quadColor.frag");

		// size, squaresNumber, tilesNumber
		_terrain = std::make_shared<Terrain>(32, 1024, 64);
		_cameraMover = std::make_shared<FreeCameraTerrainMover>(_terrain);
		_invertedCameraMover = std::make_shared<InvertedFreeCameraTerrainMover>(_terrain);

		_ground = makeGridCopy(_terrain);

		_shader = std::make_shared<ShaderProgram>("496NurgalievaData/shader.vert", "496NurgalievaData/shader.frag");

		//=========================================================
		//������������� �������� ���������� ��������
		_lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
		_lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
		_lightSpecularColor = glm::vec3(0.5, 0.5, 0.5);

		//=========================================================
		//�������� � �������� �������
		_grassTex = loadTexture("496NurgalievaData/grassy.jpg");
		_mudTex = loadTexture("496NurgalievaData/lava.jpg");
		_pathTex = loadTexture("496NurgalievaData/ground.jpg");
		_snowTex = loadTexture("496NurgalievaData/snow.jpg");
		_rockTex = loadTexture("496NurgalievaData/rock2.jpg");
		_maskTex = loadTexture("496NurgalievaData/mask6.png");

		//=========================================================
		//������������� ��������, �������, ������� ������ ��������� ������ �� ��������
		glGenSamplers(1, &_samplerGrass); // grass
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerMask); //mask
		glSamplerParameteri(_samplerMask, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerMask, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerWater); //water
		glSamplerParameteri(_samplerWater, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerWater, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerWater, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerWater, GL_TEXTURE_WRAP_T, GL_REPEAT);
		//glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		

		//_fbReflectionCamera.viewMatrix = glm::lookAt(glm::vec3(5.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f));
		//_fbReflectionCamera.projMatrix = glm::perspective(glm::radians(45.0f), 1.0f, 0.1f, 100.f);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			ImGui::Checkbox("Refraction", &_showDebugQuadRefraction);
			ImGui::Checkbox("Reflection", &_showDebugQuadReflection);
			ImGui::Checkbox("Reflection norm", &_showDebugQuadReflectionNorm);
		}
		ImGui::End();
	}

	void draw() override
	{
		unsigned t = (std::time(0) % 10) * 0.1;
		_move += _moveSpeed * cos(t);

		skyRotation += skyRotationSpeed * cos(t);

		drawToFramebuffer(_camera, _framebufferRefractionId, -1);
		drawToFramebuffer(_invertedCamera, _framebufferReflectionId, 1);
		drawToFramebuffer(_camera, _framebufferReflectionNormId, 1);
		drawScene();
	}

	void drawToFramebuffer(const CameraInfo& camera, GLuint framebufferId, int highDir)
	{

		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);

		glViewport(0, 0, _fbWidth, _fbHeight);

		glClearColor(1, 1, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//====== ������ ��� � ���������� ��������� ======
		{
			_skyboxShader->use();

			glm::vec3 cameraPos = glm::vec3(glm::inverse(camera.viewMatrix)[3]); //��������� �� ������� ���� ��������� ����������� ������ � ������� ������� ���������

			_skyboxShader->setVec3Uniform("cameraPos", cameraPos);
			_skyboxShader->setMat4Uniform("viewMatrix", glm::rotate(camera.viewMatrix, skyRotation, glm::vec3(0, 0, 1)));
			_skyboxShader->setMat4Uniform("projectionMatrix", camera.projMatrix);

			//��� �������������� ��������� � ���������� ���������� ����� ����������� �������
			glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			_skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

			glBindSampler(5, _cubeTexSampler);
			glActiveTexture(GL_TEXTURE5);  //���������� ���� 5           
			_cubeTex->bind();
			_skyboxShader->setIntUniform("cubeTex", 5);

			glDepthMask(GL_FALSE); //��������� ������ � ����� �������

			_backgroundCube->draw();

			glDepthMask(GL_TRUE); //�������� ������� ������ � ����� �������
		}

		glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));

		_shader->use();

		_shader->setVec3Uniform("light.dir", lightDir);
		_shader->setVec3Uniform("light.La", _lightAmbientColor);
		_shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
		_shader->setVec3Uniform("light.Ls", _lightSpecularColor);

		_shader->setVec4Uniform("plane", glm::vec4(0, 0, highDir, 0));


		//================ Texturing ================
		{
			GLuint textureUnitForGrassTex = 0;
			glBindSampler(textureUnitForGrassTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE0);  //���������� ���� 0 grass
			_grassTex->bind();
			_shader->setIntUniform("grassTex", textureUnitForGrassTex);

			GLuint textureUnitForPathTex = 1;
			glBindSampler(textureUnitForPathTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE1);  //���������� ���� 1 path
			_pathTex->bind();
			_shader->setIntUniform("pathTex", textureUnitForPathTex);

			GLuint textureUnitForRockTex = 2;
			glBindSampler(textureUnitForRockTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE2);  //���������� ���� 2 rock
			_rockTex->bind();
			_shader->setIntUniform("rockTex", textureUnitForRockTex);

			GLuint textureUnitForMaskTex = 3;
			glBindSampler(textureUnitForMaskTex, _samplerMask);
			glActiveTexture(GL_TEXTURE3);  //���������� ���� 3 mask
			_maskTex->bind();
			_shader->setIntUniform("maskTex", textureUnitForMaskTex);

			GLuint textureUnitForMudTex = 4;
			glBindSampler(textureUnitForMudTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE4);  //���������� ���� 4 mud
			_mudTex->bind();
			_shader->setIntUniform("mudTex", textureUnitForMudTex);
		}
		
		_shader->setIntUniform("tilesNumber", _terrain->getTilesNum());
		//_shader->setFloatUniform("squaresNumber", _terrain->getSquaresNum());

		_shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix",
			glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _ground->modelMatrix()))));

		_ground->draw();

		//����������� ������� � ��������� ���������
		glBindSampler(0, 0);
		glBindSampler(4, 0);
		glBindSampler(3, 0);
		glBindSampler(1, 0);
		glBindSampler(2, 0);

		glUseProgram(0);

		//����������� ����������, ����� ������ ��������� �� �����
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void drawScene()
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//������� ������ ����� � ������� �� ����������� ���������� ����������� �����
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//====== ������ ��� � ���������� ��������� ======
		{
		_skyboxShader->use();

		glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]); //��������� �� ������� ���� ��������� ����������� ������ � ������� ������� ���������

		_skyboxShader->setVec3Uniform("cameraPos", cameraPos);
		//_skyboxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_skyboxShader->setMat4Uniform("viewMatrix", glm::rotate(_camera.viewMatrix, skyRotation, glm::vec3(0, 0, 1)));

		_skyboxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//��� �������������� ��������� � ���������� ���������� ����� ����������� �������
		glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		_skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

		glBindSampler(5, _cubeTexSampler);
		glActiveTexture(GL_TEXTURE5);  //���������� ���� 5           
		_cubeTex->bind();
		_skyboxShader->setIntUniform("cubeTex", 5);

		glDepthMask(GL_FALSE); //��������� ������ � ����� �������

		_backgroundCube->draw();

		glDepthMask(GL_TRUE); //�������� ������� ������ � ����� �������
		}

		// ---------------- ������ ���� ----------------------
		{
			_waterShader->use();
			
			
				//_waterShader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
			_waterShader->setMat4Uniform("modelMatrix", _water->modelMatrix());
			_waterShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
			_waterShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

			_waterShader->setFloatUniform("curCameraHeight", _cameraMover->getCurrentHeight());

			glBindSampler(8, _samplerWater);
			glActiveTexture(GL_TEXTURE8);  //���������� ���� 8  
			_reflectionTex->bind();
			_waterShader->setIntUniform("reflectionTex", 8);

			glBindSampler(9, _samplerWater);
			glActiveTexture(GL_TEXTURE9);  //���������� ���� 9           
			_refractionTex->bind();
			_waterShader->setIntUniform("refractionTex", 9);

			glBindSampler(10, _samplerWater);
			glActiveTexture(GL_TEXTURE10);  //���������� ���� 10          
			_dudvMapTex->bind();
			_waterShader->setIntUniform("dudvMap", 10);

			glBindSampler(11, _samplerWater);
			glActiveTexture(GL_TEXTURE11);  //���������� ���� 8  
			_reflectionNormTex->bind();
			_waterShader->setIntUniform("reflectionNormTex", 11);

			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			_waterShader->setFloatUniform("move", _move);
			
			_water->draw();

		}

		glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));

		_shader->use();

		_shader->setVec3Uniform("light.dir", lightDir);
		_shader->setVec3Uniform("light.La", _lightAmbientColor);
		_shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
		_shader->setVec3Uniform("light.Ls", _lightSpecularColor);
		
		_shader->setVec4Uniform("plane", glm::vec4(0, 0, 0, 500));

		//================ Texturing ================
		{
			GLuint textureUnitForGrassTex = 0;
			glBindSampler(textureUnitForGrassTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE0);  //���������� ���� 0 grass
			_grassTex->bind();
			_shader->setIntUniform("grassTex", textureUnitForGrassTex);

			GLuint textureUnitForPathTex = 1;
			glBindSampler(textureUnitForPathTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE1);  //���������� ���� 1 path
			_pathTex->bind();
			_shader->setIntUniform("pathTex", textureUnitForPathTex);

			GLuint textureUnitForRockTex = 2;
			glBindSampler(textureUnitForRockTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE2);  //���������� ���� 2 rock
			_rockTex->bind();
			_shader->setIntUniform("rockTex", textureUnitForRockTex);

			GLuint textureUnitForMaskTex = 3;
			glBindSampler(textureUnitForMaskTex, _samplerMask);
			glActiveTexture(GL_TEXTURE3);  //���������� ���� 3 mask
			_maskTex->bind();
			_shader->setIntUniform("maskTex", textureUnitForMaskTex);

			GLuint textureUnitForMudTex = 4;
			glBindSampler(textureUnitForMudTex, _samplerGrass);
			glActiveTexture(GL_TEXTURE4);  //���������� ���� 4 mud
			_mudTex->bind();
			_shader->setIntUniform("mudTex", textureUnitForMudTex);
		}
		
		//_shader->setIntUniform("reflectionTex", 8);
		//��� �������������� ��������� � ���������� ���������� ����� ����������� �������
		//glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		//_shader->setMat3Uniform("textureMatrix", textureMatrix);
		//_shader->setIntUniform("cubeTex", 5);

		_shader->setIntUniform("tilesNumber", _terrain->getTilesNum());
		//_shader->setFloatUniform("squaresNumber", _terrain->getSquaresNum());

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix",
			glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
		_ground->draw();

		if (_showDebugQuadReflection)
		{
			_quadShader->use();

			glActiveTexture(GL_TEXTURE6);  //���������� ���� 6
			glBindTexture(GL_TEXTURE_2D, _renderReflectionTexId);
			glBindSampler(6, _samplerGrass);
			_quadShader->setIntUniform("tex", 6);

			glViewport(0, 0, 500, 500);

			_quad->draw();
		}

		if (_showDebugQuadReflectionNorm)
		{
			_quadShader->use();

			glActiveTexture(GL_TEXTURE12);  //���������� ���� 6
			glBindTexture(GL_TEXTURE_2D, _renderReflectionNormTexId);
			glBindSampler(12, _samplerGrass);
			_quadShader->setIntUniform("tex", 12);

			glViewport(1000, 0, 500, 500);

			_quad->draw();
		}

		if (_showDebugQuadRefraction)
		{
			_quadShader->use();

			glActiveTexture(GL_TEXTURE7);  //���������� ���� 6
			glBindTexture(GL_TEXTURE_2D, _renderRefractionTexId);
			glBindSampler(7, _samplerGrass);
			_quadShader->setIntUniform("tex", 7);

			glViewport(500, 0, 500, 500);

			_quad->draw();
		}

		//����������� ������� � ��������� ���������
		glBindSampler(0, 0);
		glBindSampler(4, 0);
		glBindSampler(3, 0);
		glBindSampler(1, 0);
		glBindSampler(2, 0);

		glUseProgram(0);
	}

private:

	TerrainPtr _terrain;

};

int main()
{
	TerrainApplication app;
	app.start();
	//system("pause");
	return 0;
}