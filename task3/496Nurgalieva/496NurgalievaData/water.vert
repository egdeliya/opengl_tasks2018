#version 330

layout(location = 0) in vec3 vertexPosition; //���������� ������� � ��������� ������� ���������
//layout(location = 1) in vec2 textureCoordinate; //���������� ������� � ��������� ������� ���������

out vec4 clipSpace;
out vec4 worldPos;
out vec2 texCoord;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

void main() {
	worldPos = modelMatrix * vec4(vertexPosition, 1.0);
	clipSpace = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
	texCoord = vec2(vertexPosition.x / 2.0 + 0.5, vertexPosition.y / 2.0 + 0.5);
	gl_Position = clipSpace;
}
