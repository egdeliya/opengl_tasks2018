#version 330

in vec4 clipSpace;
in vec2 texCoord;
in vec4 worldPos;

out vec4 outColor;

uniform sampler2D reflectionTex;
uniform sampler2D reflectionNormTex;
uniform sampler2D refractionTex;
uniform sampler2D dudvMap;

uniform float move;
uniform float curCameraHeight;

void main() {

	vec2 normalDeviceSpace = (clipSpace.xy / clipSpace.w) / 2.0 + 0.5;
	vec2 reflectionTexCoord;
	reflectionTexCoord = vec2(1.0 - normalDeviceSpace.x, normalDeviceSpace.y);

	vec2 dist1 = (texture(dudvMap, vec2(texCoord.x + move, texCoord.y)).rg * 2.0 - 1.0) * 0.02;
	vec2 dist2 = (texture(dudvMap, vec2(-texCoord.x, texCoord.y + move)).rg * 2.0 - 1.0) * 0.02;
	vec2 totalDist = dist1 + dist2;
	
	reflectionTexCoord += totalDist;
	normalDeviceSpace += totalDist;

	vec4 refractionTextColor = texture(refractionTex, normalDeviceSpace);
	vec4 reflectionTextColor;

	if (curCameraHeight >= 0) {
		reflectionTextColor = texture(reflectionTex, reflectionTexCoord);
	} else {
		reflectionTextColor = texture(reflectionNormTex, normalDeviceSpace);
	}

	outColor = mix(reflectionTextColor, refractionTextColor, 0.5) ;
	//outColor = mix(outColor, vec4(0.0, 0.2, 0.5, 0.8), 0.1) ;
	//outColor = vec4(0.0, 0.2, 0.5, 0.8);

}
